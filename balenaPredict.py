
import argparse
import sys
import balenaCheCanta

parser = argparse.ArgumentParser(description='Test')
parser.add_argument('-load', default='', 
                        help='Load weights and model topology from file')
parser.add_argument('-file', default='', 
                        help='Input file')
args = parser.parse_args()

if not args.file:
    print('ERROR: must supply -file FILE')
    sys.exit(1)

if not args.load:
    print('ERROR: must supply -load MODEL')
    sys.exit(2)

bcc = balenaCheCanta.BalenaCheCanta()
bcc.loadModel(args.load)
res = bcc.predict(args.file)
print('category probability')
for cat, prob in res[1].items():
    print('{}: {:.3f}'.format(cat, prob))
