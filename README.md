# balenaCheCanta

A project to classify recordings of whale sounds

## Initial setup

Get the code
```
git clone https://gitlab.com/alexander5/balenachecanta.git
cd balenachecanta
```

Get the data
```
wget https://www.dropbox.com/s/lkyldajpaao6feo/balenaCheCantaData.tar.bz2?dl=0
mv mv balenaCheCantaData.tar.bz2\?dl\=0 balenaCheCantaData.tar.bz2
tar xf balenaCheCantaData.tar.bz2
```

You will also need to have "opencv-python", "tensorflow" and "matplotlib" installed.

## Setup for mahuika

```
pip install opencv-python --user
ml TensorFlow/1.10.1-gimkl-2017a-Python-3.6.3
```

## How to train

```
python balenaTrain.py -h
```
for a list of command line options.

For example:
```
python balenaTrain.py -dir train_data2 -suffix png -epochs 20 -filters "8, 8, 8" -sizes "100,100" -save myModel -plotFailures
```

To run the same training on mahuika using the GPU:
```
srun --mem=3G --time=00:10:00 --partition=gpu --gres=gpu:1 python balenaTrain.py -dir train_data2 -suffix png -epochs 20 -filters "8, 8, 8" -sizes "100,100" -save myModel
```

## How to retrain

It is possible to continuously train the model by saving and loading a model. For instance:
```
python balenaTrain.py -dir train_data2 -epochs 10 -load myModel -plotFailures
```
will add 10 more epochs to the previously save "myModel".


## How to predict 

Be sure to save the model after training (option `-save SAVE`). Once saved, the model can be loaded and you can then 
classify a picture. For instance:
```
python balenaPredict.py -load myModel \
-file train_data2/Cuvier/CuvierAMAR490.9.20161102T065558Z.wav_61.9100336596558_Spectrogram_FM.png 
```
will print:
```
39kHz: 0.006
53kHz: 0.000
Cuvier: 0.994
```
meaning that there is a 99.4 percent probability that the picture is a "Cuvier".
 