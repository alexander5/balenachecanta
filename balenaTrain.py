
import argparse
import re
import numpy
import balenaCheCanta

parser = argparse.ArgumentParser(description='Test')
parser.add_argument('-dir', default='./train_data2', 
                        help='Top directory containing data')
parser.add_argument('-suffix', default='png', 
                        help='Image type or suffix, e.g. "png"')
parser.add_argument('-epochs', type=int, default=40, 
                        help='Number of training epochs or number of times the entire set of data is applied')
parser.add_argument('-filters', default='(8,8,8)', 
                        help='Number of filters for each layer, e.g. "8,8,8"')
parser.add_argument('-seed', type=int, default=123456, 
                        help='Random generator seed')
parser.add_argument('-sizes', default='(100,100)', 
                        help='Image size used for training, e.g. "100,100"')
parser.add_argument('-save', default='', 
                        help='Save weights and model topology to file (.h5 will be added), e.g. "results"')
parser.add_argument('-load', default='', 
                        help='Load weights and model topology from file')
parser.add_argument('-plotFailures', dest='plotFailures', action='store_true', 
                        help='Plot data for which the prediction was wrong')
parser.add_argument('-printModel', dest='printModel', action='store_true', 
                        help='Print model topology')
parser.add_argument('-select', default="Cuvier:20, 39kHz:40, 53kHz:50",
                     help='Randomly select training samples for each category, e.g. "Cuvier:20, 39kHz:40, 53kHz:50"')
args = parser.parse_args()

numpy.random.seed(args.seed)


bcc = balenaCheCanta.BalenaCheCanta(sizes=eval(args.sizes))
bcc.readAllFiles(dirs=[args.dir + '/Cuvier', 
                           args.dir + '/39kHz', 
                           args.dir + '/53kHz'], suffix=args.suffix)

selection = []
for sel in args.select.split(','):
    category, v = sel.split(':')
    category = re.sub(r'\s*', '', category)
    n = int(v) 
    selection.append((category, n))
data, labels = bcc.randomSelect(labelNumSamplePairs=selection)

if args.load:
    bcc.loadModel(args.load)
else:
    if args.filters.find(',') < 0:
        args.filters += ','
    bcc.buildModel(numCategories=3, numFilters=eval(args.filters))

if args.printModel:
    bcc.model.summary()

bcc.trainModel(data=data, labels=labels, epochs=args.epochs)

if args.save:
    bcc.saveModel(args.save)

diag = bcc.testModel()
numErrors = diag['number of errors']
numSamples = diag['number of samples']
fails = diag['failures']
n = len(fails['correct labels'])
for i in range(n):
    print('{:6d} predicted label: {}  (probability {:.3f}) != {}'.format(i, 
            fails['predicted labels'][i], fails['predicted probabilities'][i], fails['correct labels'][i]))
print('number of errors: {} over {} ({:.1f}%)'.format(numErrors, numSamples, 100.*numErrors/numSamples))

if args.plotFailures and numErrors > 0:
    bcc.plotData(fails['data'],
            labelList=fails['correct labels'],
            predictedLabelList=fails['predicted labels'])
