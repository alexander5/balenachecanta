import numpy
import h5py
import cv2
import glob
import os
import re
import math
import random
import functools
from tensorflow import keras
from matplotlib import pylab
from matplotlib import rcParams
rcParams['font.size'] = 8

class BalenaCheCanta(object):

    def __init__(self, sizes=(100, 100)):
        """
        Constructor
        """
        self.data = {}
        self.sizes = sizes
        self.model = keras.Sequential()


    def readAllFiles(self, dirs=[], suffix='png'):
        """
        Read all files and store the data in memory
        @param dirs list of directories containing image files
        @param suffix image type
        """
        for directory in dirs:
            # removing trailing '/'
            directory = re.sub(r'\/$', '', directory)
            for filename in glob.glob(directory + '/*.' + suffix):
                label = os.path.basename(directory)
                im = self._readFile(filename)
                self.data[label] = self.data.get(label, []) + [im]


    def buildModel(self, numCategories, numFilters=[16,]):
        """
        Build model
        @param numFilters number of filters in the neural network
        """

        # build the neural network model

        layerCounter = 0
        for nf in numFilters:
            if layerCounter == 0:
                # need to provide the input shape for the first layer in order to be able to 
                # save and load the model fom file
                self.model.add(keras.layers.Conv2D(nf, input_shape=(self.sizes[0], self.sizes[1], 1), 
                                               kernel_size=(3,3), strides=(1,1), 
                                               padding='same', data_format='channels_last', 
                                               activation='relu') )
            else:                
                self.model.add(keras.layers.Conv2D(nf, kernel_size=(3,3), strides=(1,1), 
                                               padding='same', data_format='channels_last', 
                                               activation='relu') )
            self.model.add( keras.layers.MaxPooling2D(pool_size=(2, 2)) )
            layerCounter += 1

        self.model.add( keras.layers.Flatten() )
        self.model.add( keras.layers.Dense(numCategories, activation='softmax') )

        self.model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', 
                           metrics=['accuracy'])


    def trainModel(self, data, labels, epochs=40):
        """
        Train the model
        @param data list of input data
        @param list of labels
        @param epochs number of epochs
        """

        # get the categories as strings
        categories = numpy.unique(labels)

        # attach an index in the range 0... len(categories) - 1 to each category
        self.catName2Index = {categories[i]: i for i in range(len(categories))}
        self.catIndex2Name = {i: categories[i] for i in range(len(categories))}

        # replace the the labels with index
        out = numpy.array([[self.catName2Index[c]] for c in labels])

        # turn the list of inputs into a numpy array
        n = len(labels)
        inp = numpy.zeros([n] + list(self.sizes) + [1], numpy.float32)
        for i in range(n):
            inp[i, :, :, 0] = data[i]

        # train the model
        self.model.fit(inp, out, epochs=epochs)


    def predict(self, filename):
        """
        Predict the type
        @param filename file name
        @return most likely type, {type: probability}
        """
        im = self._readFile(filename)
        im = im.reshape(1, self.sizes[0], self.sizes[1], 1)
        probs = self.model.predict(im)
        return self.catIndex2Name[numpy.argmax(probs)], \
               {self.catIndex2Name[i]: probs[0][i] for i in range(len(probs[0]))}

    def testModel(self):
        """
        Test the model
        @return number of errors, total number of samples
        """

        res = {
            'number of errors': 0,
            'number of samples': 0,
            'failures': {
                'data': [],
                'correct labels': [],
                'predicted labels': [],
                'predicted probabilities': []
            },
        }
        fails = res['failures']

        numErrors = 0
        numSamples = 0
        for label, dataList in self.data.items():

            n = len(dataList)
            inp = numpy.array(dataList).reshape(n, self.sizes[0], self.sizes[1], 1)
            predictions = self.model.predict(inp)

            for i in range(n):

                catIndex = numpy.argmax(predictions[i, :])
                correctCatIndex = self.catName2Index[label]

                if catIndex != correctCatIndex:
                    # failure
                    res['number of errors'] += 1
                    fails['data'].append(dataList[i])
                    fails['predicted labels'].append(self.catIndex2Name[catIndex])
                    fails['correct labels'].append(self.catIndex2Name[correctCatIndex])
                    fails['predicted probabilities'].append(max(predictions[i, :]))

                res['number of samples'] += 1

        return res


    def plotData(self, dataList, labelList=[], predictedLabelList=[]):
        """
        Plot input data
        @param data data
        @param labelList correct labels
        @param predictedLabelList  predicted label list
        """
        if not predictedLabelList:
            predictedLabelList = ['' for d in data]
        else:
            predictedLabelList = [' (' + p + ')' for p in predictedLabelList]
        if not labelList:
            labelList = ['' for d in data]

        nCols = 5
        nRows = int(math.ceil(len(dataList) / nCols))
        for i in range(len(dataList)):
            pylab.subplot(nRows, nCols, i + 1) # figures start at index 1
            pylab.imshow(dataList[i], cmap='CMRmap')
            pylab.title(labelList[i] + predictedLabelList[i])
            pylab.axis('off')

        pylab.show()


    def saveModel(self, modelName='model'):
        """
        Save model weights and topology to file
        @param modelName model name
        """
        modelFileName = re.sub(r'\.h5\s*', '', modelName) + '.h5'
        self.model.save(modelFileName)

        # save additional data
        f = h5py.File(modelFileName, 'a')
        balenaGrp = f.create_group("balenaCheCanta")
        # add picture sizes
        dSizes = balenaGrp.create_dataset("sizes", (2,), dtype=numpy.int)
        dSizes[:] = self.sizes
        # add category names
        categories = tuple(self.catName2Index.keys())
        dt = h5py.special_dtype(vlen=str)
        dCategories = balenaGrp.create_dataset("categories", (len(categories),), dtype=dt)
        dCategories[:] = categories
        f.close()


    def loadModel(self, modelName='model'):
        """
        Load model weights and topology from file
        @param modelName model name
        """
        modelName = re.sub(r'\.h5\s*', '', modelName)
        modelFileName = modelName + '.h5'
        self.model = keras.models.load_model(modelFileName)

        # load additional data
        f = h5py.File(modelFileName, 'r')
        self.sizes = tuple(f['balenaCheCanta']['sizes'][:])
        categories = f['balenaCheCanta']['categories'][:]
        f.close()
        self.catName2Index = {categories[i]: i for i in range(len(categories))}
        self.catIndex2Name = {i: categories[i] for i in range(len(categories))}


    def randomSelect(self, labelNumSamplePairs=[]):
        """
        Randomly select samples 
        @param labelNumSamplePairs [(label1, numSamples1), (label2, numSamples2), ...]
        @return data, labels where data and labels are two arrays
        """
        data = []
        labels = []
        for label, numSamples in labelNumSamplePairs:
            d = self.data[label]
            if numSamples > len(d):
                # allow for duplicate sampling
                data += random.choices(d, k=numSamples)
            else:
                # take unique samples
                data += random.sample(d, k=numSamples)
            labels += [label]*numSamples

        return data, labels

        
    def _readFile(self, filename):
        """
        Read data from file
        @param filename file name
        @return data array
        """
        if not os.path.exists(filename):
            print('ERROR: file {} does not exist'.format(filename))
            return None
        im = cv2.imread(filename)
        im = cv2.resize(im, self.sizes)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        im = numpy.array(im, numpy.float32)
        im /= im.max()
        return im




